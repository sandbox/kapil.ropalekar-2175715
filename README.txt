
Account Creator Module
------------------------
by Kapil Ropalekar,Tausif Sayyed


Description
-----------
This is a small and efficient module that creates a core Field API Textfield by 
the name of Account Creator UID in the People->Account Settings.
Whenever a new account is created this Textfield stores the UserID of the
User that created the Account.
This helps in tracking the Author that creates accounts which can be later used
in Views as-well.

Example and Need for the Module
----------
This module is useful when you have a requirement in your site where Non-admin
roles have permission to create user accounts.
Eg:
I have a teacher role who creates user accounts for students. Teachers belong
to multiple schools and we want to provide a list of student accounts to the
teacher created by him/her.
Currently this functionality is not supported in Drupal Core to identify Author
UID that creates Accounts. Hence the need for this module arose. 

Installation 
------------
1. Extract the 'account_creator' module directory into your Drupal modules
directory.

2. Go to the Administer > Site building > Modules page, and enable the module.

3. A new core Field API field named "Account Creator UID" is created at
Configuration->People->Account Settings->Manage Fields

4. Create New Account from a Non-admin role (admin/people/create).

5. After Account is created our "Account Creator UID" field gets populated by 
the UserID of the Author that created this account.

6. Use this field in Views later on as per your needs.
