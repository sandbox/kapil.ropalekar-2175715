<?php
/**
 * @file
 * Account Creator module.
 */

/**
 * Implements hook_help().
 */
function account_creator_help($path, $arg) {
  switch ($path) {

    case 'admin/help#account_creator':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This is a small and efficient module that creates a core Field API Textfield by the name of Account Creator UID in the People->Account Settings. Whenever a new account is created this Textfield stores the UserID of the User that created the Account.') . '</p>';
      $output .= '<p>' . t('This helps in tracking the Author that creates accounts which can be later used in Views as-well.') . '</p>';
      $output .= '<h3>' . t('Example and Need for the Module') . '</h3>';
      $output .= '<dl>';
      $output .= '<p>' . t('This module is useful when you have a requirement in your site where Non-admin roles have permission to create user accounts.') . '</p>';
      $output .= '<p>' . t('Eg: I have a teacher role who creates user accounts for students. Teachers belong to multiple schools and we want to provide a list of student accounts to the teacher created by him/her. Currently this functionality is not supported in Drupal Core to identify Author UID that creates Accounts. Hence the need for this module arose.') . '</p>';
      $output .= '<h3>' . t('Installation') . '</h3>';
      $output .= '<ol><li>' . t('Extract the "account_creator" module directory into your Drupal modules directory.') . '</li>';
      $output .= '<li>' . t('Go to the Administer > Site building > Modules page, and enable the module.') . '</li>';
      $output .= '<li>' . t('A new core Field API field named "Account Creator UID" is created at Configuration->People->Account Settings->Manage Fields') . '</li>';
      $output .= '<li>' . t('Create New Account from a Non-admin role (admin/people/create)') . '</li>';
      $output .= '<li>' . t('After Account is created our "Account Creator UID" field gets populated by the UserID of the Author that created this account.') . '</li>';
      $output .= '<li>' . t('Use this field in Views later on as per your needs.') . '</li></ol>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Hook_user_insert function().
 */
function account_creator_user_insert(&$edit, $account, $category) {
  global $user;
  $existing = user_load($account->uid);
  $edit = (array) $existing;
  $edit['field_account_creator_uid'][LANGUAGE_NONE][0]['value'] = $user->uid;
  user_save($existing, $edit);
}
